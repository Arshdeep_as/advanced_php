<?php

$user = [
	'first_name' => 'Davy',
	'last_name' => 'Jones',
	'email' => 'davy@gmail.com'
];

header('Content-type: application/json');

echo json_encode($user);