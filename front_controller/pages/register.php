<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<title>Bookstore</title>
	<link rel="stylesheet" type="text/css" href="css/style.css" />
</head>
<body>

<div class="container">

	<div id="header">

	<?php include APP . '/pages/inc/nav.inc.php'; ?>

	</div><!-- /#header -->

	<div class="header_img">
		<img src="images/header.jpg" />
	</div>

			<div class="search">

			<form>
				<input type="text" name="s" maxlength="255" />&nbsp;
				<input type="submit" value="search" />
			</form>
		</div>

		<hr class="clear" />

		<h1>Register</h1>

		<form action="#" method="post">
    <div>
         <label for="name">Text Input:</label>
         <input type="text" name="name" id="name" value="" tabindex="1" />
    </div>

    <div>
         <h4>Radio Button Choice</h4>

         <label for="radio-choice-1">Choice 1</label>
         <input type="radio" name="radio-choice-1" id="radio-choice-1" tabindex="2" value="choice-1" />

     <label for="radio-choice-2">Choice 2</label>
         <input type="radio" name="radio-choice-2" id="radio-choice-2" tabindex="3" value="choice-2" />
    </div>

  <div>
    <label for="select-choice">Select Dropdown Choice:</label>
    <select name="select-choice" id="select-choice">
      <option value="Choice 1">Choice 1</option>
      <option value="Choice 2">Choice 2</option>
      <option value="Choice 3">Choice 3</option>
    </select>
  </div>

  <div>
    <label for="textarea">Textarea:</label>
    <textarea cols="40" rows="8" name="textarea" id="textarea"></textarea>
  </div>

  <div>
      <label for="checkbox">Checkbox:</label>
    <input type="checkbox" name="checkbox" id="checkbox" />
    </div>

  <div>
      <input type="submit" value="Submit" />
    </div>
</form>

</div<!-- /.container -->

</body>
</html>