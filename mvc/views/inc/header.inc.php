<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <title><?=$title;?></title>
  <link rel="stylesheet" type="text/css" href="css/style.css" />
  
  <?php if($title == "Detail"){
    include APP.'/../views/inc/detail_style.inc.php';
  }
  ?>
  
</head>
<body>

<div class="container">
  
  <div id="header">
    
    <?php include APP.'/../views/inc/nav.inc.php'; ?>
  
  </div><!-- /#header -->

<?php if(($title == "Books")||($title == "About")||($title == "Contact")||($title == "Detail")){
  include APP.'/../views/inc/hero_image.inc.php';
}
?>