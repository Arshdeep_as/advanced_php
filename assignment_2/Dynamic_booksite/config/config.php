<?php
  /**
   *Description
   *@filename config.php
   *@author Arshdeep Singh <arshdeep6445.as@gmail.com>
   *@created_at 2018-08-02
   */

//Start output buffering.
  ob_start();
  
// Start a session
  session_start();
  
// Displaying errors.
  ini_set('display_errors', 1);
  ini_set('error_reporting', E_ALL);

// Database credentials.
  define('DB_USER', 'web_user');
  define('DB_PASS', 'mypass');
  define('DB_DSN', 'mysql:host=localhost;dbname=booksite');
  
  $dbh = new PDO(DB_DSN, DB_USER, DB_PASS);
  $dbh -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);