<?php

//authors.php
$dbh = new PDO('sqlite:database1.sqlite');
$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);


//else select all authors
$query = 'SELECT * FROM author';
$stmt = $dbh->prepare($query);
$stmt->execute();
$result = $stmt->fetchAll(PDO::FETCH_ASSOC);


header('Content-type: application/json');

echo json_encode($result);


