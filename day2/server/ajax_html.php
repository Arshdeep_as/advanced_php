<?php
  /**
   * Created by PhpStorm.
   * User: Arshdeep singh
   * Date: 27-08-2018
   * Time: 01:24 PM
   */
  
  $html = <<<EOT
  <h1>Filler Rama Presents</h1>
  <p>Where'd you get the coconuts? But you are dressed as one… Burn her! Now, look here, my good man. Oh! Come and see the violence inherent in the system! Help, help, I'm being repressed!</p>
  <p>The nose? Well, we did do the nose. Well, Mercia's a temperate zone! Well, we did do the nose. Bring her forward!</p>
  <p>The Knights Who Say Ni demand a sacrifice! I have to push the pram a lot. How do you know she is a witch? Look, my liege! The Lady of the Lake, her arm clad in the purest shimmering samite, held aloft Excalibur from the bosom of the water, signifying by divine providence that I, Arthur, was to carry Excalibur. That is why I am your king.</p>
  <p>Found them? In Mercia?! The coconut's tropical! She looks like one. You don't vote for kings. Listen. Strange women lying in ponds distributing swords is no basis for a system of government. Supreme executive power derives from a mandate from the masses, not from some farcical aquatic ceremony.</p>
EOT;

  echo $html;