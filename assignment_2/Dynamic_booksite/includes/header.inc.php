<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <title><?=$title;?></title>
  <link rel="stylesheet" type="text/css" href="css/style.css" />
  <script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.3.1.min.js"></script>
  <?php if($title == "Detail"){
    include __DIR__.'/../includes/detail_style.inc.php';
  }
  ?>

  <script>
    $(document).ready(function(){
      getPressedKey();
    });//end of document.ready function

    function getPressedKey(){
      $('#search_form input').keyup(function(e){
        e.preventDefault();
        var data = {};
        data.search = $('#search').val();
        console.log(data.search);

        getBookInfo(data);
      });//end form input keyup function
    }//end getPressedKey Function

    function getBookInfo(data){
      $.post('../server/search_form.php', data, function(response){
        console.log(response);
        showBookList(response);
      });//end .post function

    }//end getBookInfo Function

    function showBookList(data){
      var content = '<ul>';
      for(var i=0; i<data.length; i++){
        //console.log(data[i].book_id);
        content += '<li data-id="'+ data[i].book_id +'">'+ data[i].title +'</li>';
      }
      content +='</ul>';
      $('#list').html(content);

      handleClicks();

    }//end showBookList Function

    function handleClicks(){
      $('#list li').each(function(){
        $(this).click(function(){
          console.log($(this).attr('data-id'));
          var book_id = $(this).attr('data-id');
          location.href = 'detail.php?book_id='+book_id;
        });// end .click function
      });//end .each function
    }//end function handle clicks.


  </script>

  
</head>
<body>

<div class="container">
  
  <div id="header">
    
    <nav>
      <img id="logo" src="images/logo.jpg" alt="logo" />
      <ul>
        <li class="current"><a href="index.php">home</a></li><li>
          <a href="books.php">books</a></li><li>
          <a href="about.php">about</a></li><li>
          <a href="contact.php">contact</a></li>
      </ul>
    
    </nav>
  
  </div><!-- /#header -->

<?php if(($title == "Books")||($title == "About")||($title == "Contact")||($title == "Detail")){
  include __DIR__.'/../includes/hero_image.inc.php';
}
?>