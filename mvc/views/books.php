<?php
  include APP . '/../views/inc/header.inc.php';
?>
		<h1><?=$title;?></h1>
    <?php
      if(!empty($_SESSION['cart'])){
        include '../includes/cart.inc.php';
      }
    ?>
	<div class="categories">

		<h3>Categories</h3>

		<ul>
      
      <?php foreach($genres as $row) : ?>
        <li><a href="books.php?genre=<?=$row['name'];?>"><?=$row['name'];?></a></li>
      <?php endforeach; ?>
      
		</ul>

	</div>
 
	<div class="shelf">
    <?php if(!empty($books)) :?>
      
      <?php foreach($books as $row) : ?>
        <div class="book">
          <div class="img">
            <img src="images/covers/<?=$row['image'];?>" alt="<?=$row['title'];?>" />
          </div>
          <div class="details">
            <p><strong><?=$row['title'];?></strong><br />
              by <a href="/?page=detail&books&author=<?=$row['author_id'];?>"><?=$row['author'];?></a><br />
              <span><?=$row['genre'];?></span>, <?=$row['num_pages'];?> pages, $<?=$row['price'];?></p>
            <?=$row['description'];?>
              <p><a class=more" href="/?page=detail&book_id=<?=$row['book_id'];?>">More info</a>
          </div>
        </div><!-- /.book -->
      <?php endforeach; ?>
      
   <?php else : ?>
      
      <h2>Sorry, No Records by this name.</h2>
  
    <?php endif; ?>
	</div><!-- /.shelf -->

</div<!-- /.container -->

<?php  
  include APP.'/../views/inc/footer.inc.php';
?>