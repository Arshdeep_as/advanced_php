<?php
  
  $title = "Detail";
  

  require  APP . '/../models/genre_db.php';
  require  APP . '/../models/book_more_info.php';
  require  APP . '/../models/book_same_publisher.php';
  
  
  $genres = getGenres($dbh);
  
  if(!empty($_GET)){
    
    $book_info = getBooksMoreInfo($dbh, $_GET['book_id']);
    //var_dump($book_info);
    
    $same_publisher = getBooksBySamePublisher($dbh, $book_info['publisher_id']);
    //var_dump($same_publisher);
  }

  require  APP . '/../views/detail.php';