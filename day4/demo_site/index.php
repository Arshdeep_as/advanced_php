<!DOCTYPE html>
<html>
<head>
	<title>index</title>
	<script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.3.1.min.js"></script>

	<script>
		$(document).ready(function(){
			$('#content').load('inc/home.html');
			location.hash='home';
			$('#main').load('inc/nav.html', null, pressed);
		});

		var pressed = function(){
			$('#nav li a').click(function(e){
				e.preventDefault();
				console.log($(this).attr('href'));
				var page_requested = $(this).attr('href');
				page_requested = page_requested.substring(1, page_requested.length);
				console.log('Requested page is: '+page_requested);
				$('#content').load('inc/'+page_requested+'.html');
				location.hash = page_requested;
   				/*var x = "The anchor part is now: " + location.hash;
   				console.log(x);*/
			});
		}


	</script>
</head>
<body>
	<div id="main">
	</div>
	<div id="content">
	</div>
</body>
</html>