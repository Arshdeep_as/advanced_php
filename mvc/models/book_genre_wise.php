<?php
  /**
   * Created by PhpStorm.
   * User: Arshdeep singh
   * Date: 10-08-2018
   * Time: 11:21 AM
   */
  function getBooksAsPreGenre($dbh, $genre)
  {
    //create query
    $query = 'SELECT book_id as book_id, book.title, book.year_published, book.num_pages, book.price, book.image, book.description, author.author_id, author.name as author, genre.name as genre FROM book JOIN author USING(author_id) JOIN genre USING(genre_id) WHERE genre.name = :genre_name';
    
    //Prepare query
    $stmt = $dbh -> prepare($query);
    $stmt->bindValue(':genre_name', $genre, PDO::PARAM_STR);
    //Execute query
    $stmt -> execute();
    
    //Fetch results
    return $stmt -> fetchAll(PDO::FETCH_ASSOC);
  }