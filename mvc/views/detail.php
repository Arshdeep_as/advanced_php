<?php
  include APP . '/../views/inc/header.inc.php';
?>
		<h1>Detail</h1>

  <?php
    if(!empty($_SESSION['cart'])){
      include APP . '/../views/inc/cart.inc.php';
    }
  ?>

	<div class="categories">

		<h3>Categories</h3>

		<ul>
      <?php foreach($genres as $row) : ?>
        <li><a href="/?page=books&genre=<?=$row['name'];?>"><?=$row['name'];?></a></li>
      <?php endforeach; ?>
		</ul>

	</div>
  
  
	
  <div class="shelf">

		<div class="book_cover">

			<img src="images/covers/<?=$book_info['image'];?>" alt="<?=$book_info['title'];?>" />

		</div><!-- /.book_cover -->

		<div class="book_details">

			<h3><?=$book_info['title'];?></h3>

			<ul>
				<li><strong>Title</strong>: <?=$book_info['title'];?></li>
				<li><strong>Author</strong>: <?=$book_info['author'];?></li>
				<li><strong>Genre</strong>: <?=$book_info['genre'];?></li>
				<li><strong>Format</strong>: <?=$book_info['format'];?></li>
				<li><strong>Number of Pages</strong>: <?=$book_info['num_pages'];?></li>
				<li><strong>Year Published</strong>: <?=$book_info['year_published'];?></li>
				<li><strong>In Print</strong>: <?=($book_info['in_print'])? 'Yes' : 'No';?></li>
				<li><strong>Price</strong>: $<?=$book_info['price'];?></li>
				<li><strong>Publisher</strong>: <?=$book_info['publisher'];?></li>
				<li><strong>Publisher City</strong>: <?=$book_info['city'];?></li>
        
        <li><form action="cart.php" method="post">
            
            <input type="hidden" name="book_id" value="<?=$book_info['book_id'];?>"/>
            <button type="submit">Add to Cart</button>
            
          </form> </li>
			</ul>

		</div><!-- /.book_details -->

		<div class="book_author">

			<h4>Meet the author...</h4>

			<h5><?=$book_info['author'];?></h5>

			<img src="images/authors/<?=str_replace(' ','_', $book_info['author']);?>.jpg" alt="<?=$book_info['author'];?>" />

			<p><?=$book_info['author'];?>.  Country: <?=$book_info['country'];?></p>

			<img src="images/countries/<?=$book_info['country'];?>.jpg" alt="<?=$book_info['country'];?> Flag" />

			<p>View <a href="/?page=books&author=<?=$book_info['author_id'];?>">other books by this author</a>.</p>

		</div><!-- /.book_author -->

		<div class="book_description">
						<h4>Description</h4>
            <?=$book_info['description'];?>
		</div>

		<div class="book_publisher">

			<h3>Other books by this publisher</h3>

      <?php foreach($same_publisher as $row) : ?>
			<div class="book_item">
				<a href="/?page=detail&book_id=<?=$row['book_id'];?>"><img src="images/covers/<?=$row['image'];?>" alt="<?=$row['title'];?>" /></a>
			</div>
			<?php endforeach; ?>

		</div>

		

	</div><!-- /.shelf -->

</div<!-- /.container -->

<?php
  
  include APP . '/../views/inc/footer.inc.php';

?>
?>
		<h1>Detail</h1>

  <?php
    if(!empty($_SESSION['cart'])){
      include APP . '/../views/inc/cart.inc.php';
    }
  ?>

	<div class="categories">

		<h3>Categories</h3>

		<ul>
      <?php foreach($genres as $row) : ?>
        <li><a href="/?page=books&genre=<?=$row['name'];?>"><?=$row['name'];?></a></li>
      <?php endforeach; ?>
		</ul>

	</div>
  
  
	
  <div class="shelf">

		<div class="book_cover">

			<img src="images/covers/<?=$book_info['image'];?>" alt="<?=$book_info['title'];?>" />

		</div><!-- /.book_cover -->

		<div class="book_details">

			<h3><?=$book_info['title'];?></h3>

			<ul>
				<li><strong>Title</strong>: <?=$book_info['title'];?></li>
				<li><strong>Author</strong>: <?=$book_info['author'];?></li>
				<li><strong>Genre</strong>: <?=$book_info['genre'];?></li>
				<li><strong>Format</strong>: <?=$book_info['format'];?></li>
				<li><strong>Number of Pages</strong>: <?=$book_info['num_pages'];?></li>
				<li><strong>Year Published</strong>: <?=$book_info['year_published'];?></li>
				<li><strong>In Print</strong>: <?=($book_info['in_print'])? 'Yes' : 'No';?></li>
				<li><strong>Price</strong>: $<?=$book_info['price'];?></li>
				<li><strong>Publisher</strong>: <?=$book_info['publisher'];?></li>
				<li><strong>Publisher City</strong>: <?=$book_info['city'];?></li>
        
        <li><form action="cart.php" method="post">
            
            <input type="hidden" name="book_id" value="<?=$book_info['book_id'];?>"/>
            <button type="submit">Add to Cart</button>
            
          </form> </li>
			</ul>

		</div><!-- /.book_details -->

		<div class="book_author">

			<h4>Meet the author...</h4>

			<h5><?=$book_info['author'];?></h5>

			<img src="images/authors/<?=str_replace(' ','_', $book_info['author']);?>.jpg" alt="<?=$book_info['author'];?>" />

			<p><?=$book_info['author'];?>.  Country: <?=$book_info['country'];?></p>

			<img src="images/countries/<?=$book_info['country'];?>.jpg" alt="<?=$book_info['country'];?> Flag" />

			<p>View <a href="/?page=books&author=<?=$book_info['author_id'];?>">other books by this author</a>.</p>

		</div><!-- /.book_author -->

		<div class="book_description">
						<h4>Description</h4>
            <?=$book_info['description'];?>
		</div>

		<div class="book_publisher">

			<h3>Other books by this publisher</h3>

      <?php foreach($same_publisher as $row) : ?>
			<div class="book_item">
				<a href="/?page=detail&book_id=<?=$row['book_id'];?>"><img src="images/covers/<?=$row['image'];?>" alt="<?=$row['title'];?>" /></a>
			</div>
			<?php endforeach; ?>

		</div>

		

	</div><!-- /.shelf -->

</div<!-- /.container -->

<?php
  
  include APP . '/../views/inc/footer.inc.php';

?>