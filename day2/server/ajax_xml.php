<?php
  /**
   * Created by PhpStorm.
   * User: Arshdeep singh
   * Date: 27-08-2018
   * Time: 02:37 PM
   */
  
  require 'ArrayToXml.php';
  
  $user = [
    'first_name' => 'Dave',
    'last_name' => 'Jones',
    'age' => '22',
    'email' => 'dave@example.com',
    'hobbies' => ['reading', 'writing', 'rithmatic']
  ];
  
  $x = new ArrayToXml($user, 'user');
  
  $xml = $x->toXml();
  
  header('Content-type: application/xml');
  
  echo $xml;