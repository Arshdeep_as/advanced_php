<?php

//process_form.php

function string($value){
	return preg_match('/^([A-z\s\-\']+)$/', $value);
}

if($_SERVER['REQUEST_METHOD'] == 'POST'){
	$errors = [];
	$validator = [
		'first_name' => 'string',
		'last_name' => 'string',
		'email' => 'email',
		'password' => 'strength',
		'confirm_password' => 'match'
	];

	//array_key_exist($key , $validator);
	//call_user_func($func_name, $params);

	foreach($_POST as $key => $value){
		if(array_key_exists($key, $validator)){
			//$validator[$key]($value);
			//$errors[$key] = !call_user_func($validator[$key], $value);
			if(call_user_func($validator[$key], $value)){
				$errors[$key] = 0;
			}
			else{
				$errors[$key] = 1;				
			}
		}
	}
	//print_r($_POST);

	header('Content-type: application/json');
	echo json_encode($errors);
}