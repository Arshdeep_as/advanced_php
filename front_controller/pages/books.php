<?php 

	$books = getRandomBooks($dbh, 5);
	$genres = getGenres($dbh);

?><!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<title>Bookstore</title>
	<link rel="stylesheet" type="text/css" href="css/style.css" />
</head>
<body>

<div class="container">

	<div id="header">

	<?php include APP . '/pages/inc/nav.inc.php'; ?>

	</div><!-- /#header -->

	<div class="header_img">
		<img src="images/header.jpg" />
	</div>

			<div class="search">

			<form>
				<input type="text" name="s" maxlength="255" />&nbsp;
				<input type="submit" value="search" />
			</form>
		</div>

		<hr class="clear" />

		<h1>Books</h1>

	<div class="categories">

		<h3>Categories</h3>

		<ul>
			<?php foreach($genres as $row) : ?>
				<li><a href=""><?=$row['name']?></a></li>
			<?php endforeach;?>
		</ul>

	</div>

	<div class="shelf">

	<?php if(!empty($books)) :?>
      
      <?php foreach($books as $row) : ?>
        <div class="book">
          <div class="img">
            <img src="images/covers/<?=$row['image'];?>" alt="<?=$row['title'];?>" />
          </div>
          <div class="details">
            <p><strong><?=$row['title'];?></strong><br />
              by <a href="books.php?author=<?=$row['author_id'];?>"><?=$row['author'];?></a><br />
              <span><?=$row['genre'];?></span>, <?=$row['num_pages'];?> pages, $<?=$row['price'];?></p>
            <?=$row['description'];?>
              <p><a class=more" href="detail.php?book_id=<?=$row['book_id'];?>">More info</a>
          </div>
        </div><!-- /.book -->
      <?php endforeach; ?>
      
   <?php else : ?>
      
      <h2>Sorry, No Records by this name.</h2>
  
    <?php endif; ?>

	</div><!-- /.shelf -->

</div<!-- /.container -->

</body>
</html>