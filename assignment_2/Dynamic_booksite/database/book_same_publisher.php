<?php
  /**
   * Created by PhpStorm.
   * User: Arshdeep singh
   * Date: 10-08-2018
   * Time: 02:21 PM
   */
  function getBooksBySamePublisher($dbh, $publisher_id)
  {
    //create query
    $query = 'SELECT book.book_id as book_id, book.title, book.image FROM book WHERE publisher_id = :publisher_id';
    
    //Prepare query
    $stmt = $dbh -> prepare($query);
    $stmt->bindValue(':publisher_id', $publisher_id, PDO::PARAM_INT);
    //Execute query
    $stmt -> execute();
    
    //Fetch results
    return $stmt -> fetchAll(PDO::FETCH_ASSOC);
    
  }