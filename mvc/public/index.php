<?php

//start the ob buffering
	ob_start();

//starting the session
  session_start();

define ('APP', __DIR__ );

require APP . '/../config.php';

//echo "It Works!!!";

$allowed = ['about', 'books', 'contact', 'detail', 'home', 'login', 'register', 'cart', 'genre'];

	if(empty($_GET)){
		$_GET['page'] = 'home';
	}

	if(in_array($_GET['page'], $allowed)){
		$page_name = $_GET['page'] . '.php';
		//var_dump($page_name);
		require APP . "/../controllers/$page_name";
	}
	else{
		//header('HTTP/1.0 404 Not Found');
		//include APP . '/../controllers/error_404.php';
		die('Error 404 not found.');
	}