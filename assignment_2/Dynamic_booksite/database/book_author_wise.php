<?php
  /**
   * Created by PhpStorm.
   * User: Arshdeep singh
   * Date: 10-08-2018
   * Time: 12:59 PM
   */
  function getBooksAsPreAuthor($dbh, $author_id)
  {
    //create query
    $query = 'SELECT book_id as book_id, book.title, book.year_published, book.num_pages, book.price, book.image, book.description, author.author_id, author.name as author, genre.name as genre FROM book JOIN author USING(author_id) JOIN genre USING(genre_id) WHERE author_id = :author_id';
    
    //Prepare query
    $stmt = $dbh -> prepare($query);
    $stmt->bindValue(':author_id', $author_id, PDO::PARAM_INT);
    //Execute query
    $stmt -> execute();
    
    //Fetch results
    return $stmt -> fetchAll(PDO::FETCH_ASSOC);
  }