<?php
  /**
   * Created by PhpStorm.
   * User: Arshdeep singh
   * Date: 10-08-2018
   * Time: 01:29 PM
   */
  function getBooksMoreInfo($dbh, $book_id)
  {
    //create query
    $query = 'SELECT book.book_id as book_id, book.title, book.year_published, book.num_pages, book.price, book.in_print, book.image, book.description, author.author_id, author.name as author, author.country, genre.name as genre, publisher.name as publisher, publisher.publisher_id, publisher.city, format.name as format FROM book JOIN author USING(author_id) JOIN genre USING(genre_id) JOIN publisher USING(publisher_id) JOIN format USING(format_id) WHERE book_id = :book_id';
    
    //Prepare query
    $stmt = $dbh -> prepare($query);
    $stmt->bindValue(':book_id', $book_id, PDO::PARAM_INT);
    //Execute query
    $stmt -> execute();
    
    //Fetch results
    return $stmt -> fetch(PDO::FETCH_ASSOC);
    
  }