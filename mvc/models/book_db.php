<?php
  /**
   * Created by PhpStorm.
   * User: Arshdeep singh
   * Date: 10-08-2018
   * Time: 10:35 AM
   */
  
  function getRandomBooks($dbh, $num_books)
  {
    //create query
    $query = 'SELECT book.book_id as book_id, book.title, book.year_published, book.num_pages, book.price, book.image, book.description, book.description, author.author_id, author.name as author, genre.name as genre FROM book JOIN author USING(author_id) JOIN genre USING(genre_id) ORDER by RAND() LIMIT 5';
    
    //Prepare query
    $stmt = $dbh -> prepare($query);
    
    //Execute query
    $stmt -> execute();
    
    //Fetch results
    return $stmt -> fetchAll(PDO::FETCH_ASSOC);
    
    }