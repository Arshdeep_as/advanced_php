<?php
  
  $title = "Bookstore";
  
  $slides = array('book_club.jpg','books.jpg','meet_the_author.jpg','sf_releases.jpg');
  //array_rand -- returns random key from the array. or suffle.
  
  shuffle($slides);
  
  $src = array_pop($slides);

  //echo "home controllers";
  require APP . '/../views/home.php';