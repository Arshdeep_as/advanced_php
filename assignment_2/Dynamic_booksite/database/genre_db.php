<?php
  /**
   * Created by PhpStorm.
   * User: Arshdeep singh
   * Date: 10-08-2018
   * Time: 10:36 AM
   */
  
  function getGenres($dbh)
  {
    //create query
    $query = 'SELECT * FROM genre';
    
    //Prepare query
    $stmt = $dbh -> prepare($query);
    
    //Execute query
    $stmt -> execute();
    
    //Fetch results
    return $stmt -> fetchAll(PDO::FETCH_ASSOC);
  }