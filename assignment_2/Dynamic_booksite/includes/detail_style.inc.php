<style>
  
  .book_cover {
    width: 25%;
    float: left;
    clear: both;
  }
  
  .book_cover img {
    width: auto;
    height: auto;
    max-width: 100%;
  }
  
  .book_details {
    width: 50%;
    float: left;
    padding: 0 20px;
  }
  
  .book_author {
    width: 25%;
    float: left;
    font-size: 80%;
  }
  
  .book_author img {
    width: auto;
    height: auto;
    max-width: 100%;
  }
  
  .book_description {
    
    padding: 30px;
    border: 1px solid #cfcfcf;
    margin-bottom: 30px;
    clear: both;
  }
  
  
  
  .book_item {
    width: 15%;
    float: left;
    margin-right: 10px;
    padding: 20px;
    border: 1px solid #cfcfcf;
  }
  
  .book_item img {
    width: auto;
    height: auto;
    max-width: 100%;
  }

</style>