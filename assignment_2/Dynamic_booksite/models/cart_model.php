<?php
  /**
   * Created by PhpStorm.
   * User: Arshdeep singh
   * Date: 17-08-2018
   * Time: 11:22 AM
   */
  
  //require __DIR__. '/../config/config.php';
  
  //require  __DIR__.'/../database/book_more_info.php';
  
  define('PST', '.08');
  define('GST', '.05');
  
  /**
   * @param $dbh PDO Database connection handler
   * @param $book_id Int book id of the book you want to add to the cart.
   */

  function addToCart($dbh, $book_id)
  {
    $book = getBooksMoreInfo($dbh, $book_id);
    
    $cart = [
      'title' => $book['title'],
      'author' => $book['price'],
      'price' => $book['price'],
    ];
    
    $_SESSION['cart'] = $cart;
  }