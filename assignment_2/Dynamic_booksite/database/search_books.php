<?php
  /**
   * Created by PhpStorm.
   * User: Arshdeep singh
   * Date: 10-08-2018
   * Time: 02:53 PM
   */
  
  function searchBooks($dbh, $search_string)
  {
    //create query
    $query = 'SELECT book.book_id as book_id, book.title, book.year_published, book.num_pages, book.price, book.image, book.description, book.description, author.author_id, author.name as author, genre.name as genre FROM book JOIN author USING(author_id) JOIN genre USING(genre_id) WHERE book.title = :search OR author.name = :search';
    
    //Prepare query
    $stmt = $dbh -> prepare($query);
    $stmt->bindValue(':search', $search_string, PDO::PARAM_STR);
    //Execute query
    $stmt -> execute();
    
    //Fetch results
    return $stmt -> fetchAll(PDO::FETCH_ASSOC);
    
  }