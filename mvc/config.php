<?php

// Displaying errors.
  ini_set('display_errors', 1);
  ini_set('error_reporting', E_ALL);

// Database credentials.
	define('DB_USER', 'web_user');
	define('DB_PASS', 'mypass');
	define('DB_DSN', 'mysql:host=localhost;dbname=booksite');

// DBH controller

//connect to mysql using database handler.
	$dbh = new PDO(DB_DSN, DB_USER, DB_PASS);
//setting database handler to show errors.
	$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

//base path(current directory)
//	define('APP', __DIR__);	

