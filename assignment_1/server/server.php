<?php

//checking if in headers array, http request header is set.
if(array_key_exists('X-Requested-With',getallheaders())){

	//if it is set then connecting to the database.
	$dbh = new PDO('mysql:host=localhost;dbname=booksite','root','');
	$dbh -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

	//checking if book_id is set in the url, if not fetching the details of all books from the database.
	if(!isset($_GET['book_id'])){
		$query = 'SELECT * FROM book';
		$stmt = $dbh -> prepare($query);
		$stmt -> execute();

		$book = $stmt -> fetchAll(PDO::FETCH_ASSOC);
		//var_dump($book);
	}
	
	//if book_id is set in the url then fetching the details of the requested book from the database.
	if(!empty($_GET['book_id'])){
		$query = 'SELECT book.book_id as book_id, book.title, book.year_published, book.num_pages, book.price, book.in_print, book.image, book.description, author.author_id, author.name as author, author.country, genre.name as genre, publisher.name as publisher, publisher.publisher_id, publisher.city, format.name as format FROM book JOIN author USING(author_id) JOIN genre USING(genre_id) JOIN publisher USING(publisher_id) JOIN format USING(format_id) WHERE book_id = :book_id';
		$stmt = $dbh -> prepare($query);
		$stmt -> bindValue(':book_id', $_GET['book_id'], PDO::PARAM_INT);
		$stmt -> execute();
		$book = $stmt -> fetch(PDO::FETCH_ASSOC);
		//var_dump($book);
	}

	//setting the header content type to application Json so that while recieving it browser know what kind of response it is getting
	header('Content-type: application/json');
	//sending the encoded data back.
	echo json_encode($book);
}
else{
	die("{error:Sorry this page is not a valid AJAX request.}");
}