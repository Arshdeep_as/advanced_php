<?php
  /**
   * Created by PhpStorm.
   * User: Arshdeep singh
   * Date: 27-08-2018
   * Time: 01:50 PM
   */
  
  $user = [
    'first_name' => 'Dave',
    'last_name' => 'Jones',
    'age' => '22',
    'email' => 'dave@example.com',
    'hobbies' => ['reading', 'writing', 'rithmatic']
  ];
  
  header('Content-type: application/json');
  echo json_encode($user);