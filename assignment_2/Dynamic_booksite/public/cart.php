<?php
  
  $title = "Detail";
  
  require __DIR__ . '/../config/config.php';
  require __DIR__ . '/../database/genre_db.php';
  require __DIR__ . '/../database/book_more_info.php';
  require __DIR__ . '/../database/book_same_publisher.php';
  include __DIR__ . '/../includes/header.inc.php';
  require __DIR__ . '/../models/cart_model.php';
  
  //var_dump($_POST);
  
  //Make sure we have a post request.
  if(filter_input(INPUT_SERVER, 'REQUEST_METHOD') != 'POST'){
    die('Sorry, you are in a wrong place.');
  }
  //make sure there is  cart or make one.
  if(!isset($_SESSION['cart'])){
    $_SESSION['cart'] = array();
    
  }
  if(!empty($_POST['book_id'])){
    addToCart($dbh, $_POST['book_id']);
    header('Location:'.$_SERVER['HTTP_REFERER'] );
  }
  
?>
  
  </div<!-- /.container -->

<?php
  
  include __DIR__.'/../includes/footer.inc.php';

?>