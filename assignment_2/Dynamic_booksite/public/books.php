<?php
  
  $title = "Books";
  
  require __DIR__ . '/../config/config.php';
  include __DIR__ . '/../includes/header.inc.php';
  require __DIR__ . '/../database/genre_db.php';
  require __DIR__ . '/../database/book_db.php';
  require __DIR__ . '/../database/book_genre_wise.php';
  require __DIR__ . '/../database/book_author_wise.php';
  require __DIR__ . '/../database/search_books.php';
  
  
  $genres = getGenres($dbh);
  
  //var_dump($books);

  if(!empty($_GET['genre'])) {
    $books = getBooksAsPreGenre($dbh, $_GET['genre']);
    $title = "Books By Genre";
  }
  else if(!empty($_GET['author'])){
    $books = getBooksAsPreAuthor($dbh, $_GET['author']);
    $title = "Books By Title";
  }
  
  else if(!empty($_GET['search'])){
    //var_dump($_GET['search']);
    $books = searchBooks($dbh, $_GET['search']);
    $title = "Search Results";
  }
  else{
    $books = getRandomBooks($dbh, 5);
    $title = "Recent Best-Sellers";
  }
  //var_dump($books);
?>
		<h1><?=$title;?></h1>
    <?php
      if(!empty($_SESSION['cart'])){
        include '../includes/cart.inc.php';
      }
    ?>
	<div class="categories">

		<h3>Categories</h3>

		<ul>
      
      <?php foreach($genres as $row) : ?>
        <li><a href="books.php?genre=<?=$row['name'];?>"><?=$row['name'];?></a></li>
      <?php endforeach; ?>
      
		</ul>

	</div>
 
	<div class="shelf">
    <?php if(!empty($books)) :?>
      
      <?php foreach($books as $row) : ?>
        <div class="book">
          <div class="img">
            <img src="images/covers/<?=$row['image'];?>" alt="<?=$row['title'];?>" />
          </div>
          <div class="details">
            <p><strong><?=$row['title'];?></strong><br />
              by <a href="books.php?author=<?=$row['author_id'];?>"><?=$row['author'];?></a><br />
              <span><?=$row['genre'];?></span>, <?=$row['num_pages'];?> pages, $<?=$row['price'];?></p>
            <?=$row['description'];?>
              <p><a class=more" href="detail.php?book_id=<?=$row['book_id'];?>">More info</a>
          </div>
        </div><!-- /.book -->
      <?php endforeach; ?>
      
   <?php else : ?>
      
      <h2>Sorry, No Records by this name.</h2>
  
    <?php endif; ?>
	</div><!-- /.shelf -->

</div<!-- /.container -->

<?php
  
  include __DIR__.'/../includes/footer.inc.php';

?>