<?php
  
  $title = "Books";
  
  require APP . '/../models/genre_db.php';
  require APP . '/../models/book_db.php';
  require APP . '/../models/book_genre_wise.php';
  require APP . '/../models/book_author_wise.php';
  require APP . '/../models/search_books.php';
  
  $genres = getGenres($dbh);
  
  //var_dump($books);

  if(!empty($_GET['genre'])) {
    $books = getBooksAsPreGenre($dbh, $_GET['genre']);
    $title = "Books By Genre";
  }
  else if(!empty($_GET['author'])){
    $books = getBooksAsPreAuthor($dbh, $_GET['author']);
    $title = "Books By Title";
  }
  
  else if(!empty($_GET['search'])){
    //var_dump($_GET['search']);
    $books = searchBooks($dbh, $_GET['search']);
    $title = "Search Results";
  }
  else{
    $books = getRandomBooks($dbh, 5);
    $title = "Recent Best-Sellers";
  }
  //var_dump($books);

require APP . '/../views/books.php';