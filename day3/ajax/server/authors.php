<?php

//authors.php
$dbh = new PDO('sqlite:database1.sqlite');
$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

//var_dump($dbh);
//if get author id is set, select that author
if(!empty($_GET['author_id'])){
	$query = 'SELECT * FROM author WHERE author_id = :author_id';
	$stmt = $dbh->prepare($query);
	$stmt->bindValue(':author_id', $_GET['author_id'], PDO::PARAM_INT);
	$stmt->execute();
	$author = $stmt->fetch(PDO::FETCH_ASSOC);

	//var_dump($author);
}
else{
	//else select all authors
	$query = 'SELECT * FROM author';
	$stmt = $dbh->prepare($query);
	$stmt->execute();
	$author = $stmt->fetchAll(PDO::FETCH_ASSOC);

	//var_dump($author);
}

?><!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Authors Details</title>
</head>
<body>
	<!-- if single author detail in html -->

	<?php if(!empty($_GET['author_id'])) : ?>
		<p>Author Name: <?=$author['name']?></p>
		<p>Author City: <?=$author['country']?></p>
		<p>Image:<br /> <img src="images/<?=$author['image']?>" /></p>
	
	<!-- else -->
	<?php else: ?>
	<!-- Output author list in html -->

	<?php foreach($author as $row): ?>
		<ul>
			<li data-id="<?=$row['author_id'];?>"><?=$row['name'];?></li>
		</ul>
	<?php endforeach; ?>

	<!-- endif -->
	<?php endif; ?>
</body>
</html>

