<?php

//start the ob buffering
	ob_start();

//starting the session
  session_start();

require '../config.php';

//this can be done if we are defining app in index.php
//define('APP', __DIR__);
//require APP . '/../config.php';

	//var_dump($_GET);

	$allowed = ['about', 'books', 'contact', 'detail', 'home', 'login', 'register'];

	if(empty($_GET)){
		$_GET['page'] = 'home';
	}

	if(in_array($_GET['page'], $allowed)){
		$page_name = $_GET['page'] . '.php';
		//var_dump($page_name);
		include APP . "/pages/$page_name";
		
	}
	else{
		header('HTTP/1.0 404 Not Found');
		include APP . '/pages/error_404.php';
	}
