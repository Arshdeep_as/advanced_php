<?php

//authors.php
$dbh = new PDO('sqlite:database1.sqlite');
$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

//var_dump($dbh);
//if get author id is set, select that author
if(!empty($_GET['author_id'])){
	$query = 'SELECT * FROM author WHERE author_id = :author_id';
	$stmt = $dbh->prepare($query);
	$stmt->bindValue(':author_id', $_GET['author_id'], PDO::PARAM_INT);
	$stmt->execute();
	$result = $stmt->fetch(PDO::FETCH_ASSOC);



	//var_dump($author);
}
else{
	//else select all authors
	$query = 'SELECT * FROM author';
	$stmt = $dbh->prepare($query);
	$stmt->execute();
	$result = $stmt->fetchAll(PDO::FETCH_ASSOC);

	//var_dump($author);
}

header('Content-type: application/json');

echo json_encode($result);

?>
