<div class="cart">
  
  <?php if(!empty($_SESSION['cart'])): ?>
    <strong>Your Cart:</strong><br />
    <small>You have one item on your cart.</small><br />
    <small><?=$_SESSION['cart']['title']?>, by <?=$_SESSION['cart']['author']?> and price is <?=$_SESSION['cart']['price']?></small><br />
    <small><a href="checkout.php">Checkout now!</a></small>
  <?php endif; ?>

</div>